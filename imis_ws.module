<?php
/**
 * @file
 * iMIS Webservices module code.
 */

/**
 * Implements hook_menu().
 */
function imis_ws_menu() {
  $items['admin/config/services/imis'] = array(
    'title' => 'iMIS',
    'description' => 'Configure settings for connecting to iMIS.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('imis_ws_config_form'),
    'access arguments' => array('configure imis ws'),
    'file' => 'imis_ws.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function imis_ws_permission() {
  return array(
    'configure imis ws' => array(
      'title' => t('Configure iMIS webservices'),
    ),
  );
}

/**
 * Instantiates the proper request class to query iMIS.
 *
 * @return ImisWsRequestInterface
 */
function imis_ws_request() {
  // Statically cache method and class functionality.
  $class = &drupal_static(__FUNCTION__);

  if (!$class) {
    $class = ($config = variable_get('imis_ws_request_class')) ? $config : 'ImisWsRequest';
  }

  return new $class();
}
